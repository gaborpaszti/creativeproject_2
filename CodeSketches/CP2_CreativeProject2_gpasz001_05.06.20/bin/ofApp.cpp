#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){
	
	ofEnableDepthTest();  /// gui goes invisible dark mode when this active
	
	ofSetFrameRate(60);   /// Add this line to explicitly set the framerate to 60 frames per second
	//ofSetWindowShape(1500, 1500); /// Add this line to explicitly set the window size
	
	ofDisableArbTex();
	ofEnableNormalizedTexCoords();
	ofSetVerticalSync(true);

	

	/// OSC init
	oscReceiver.setup(PORT);
	//oscSender.setup("localhost", PORT);

	
	/// SOUND load
	/// Loading sound with setting its properties
	//themeSong.setLoopState(OF_LOOP_PALINDROME);

	soundSample0.load("MainThemeSong.mp3");
	soundSample0.setVolume(0.5f);
	soundSample0.play();                  
	soundSample0.setLoop(true);
	soundSample0.setSpeed(1.0f);
	soundSample0.setPan(0);
	soundSample0.setMultiPlay(false);

	/// #1
	soundSample1.load("swoosh.wav");
	//soundSample1.setVolume(0.5f);             ///  volume is active in the OSC Leap section
	//soundSample1.play();                     ///  play is active in the key pressed function
	soundSample1.setLoop(false);
	soundSample1.setSpeed(1.0f);
	soundSample1.setPan(0);
	soundSample1.setMultiPlay(false);
	/// #2
	soundSample2.load("flippingCintar.wav");
	//soundSample2.setVolume(0.5f);            ///  volume is active in the OSC Leap section
	//soundSample2.play();                    ///  play is active in the key pressed function
	soundSample2.setLoop(false);
	soundSample2.setSpeed(1.0f);
	soundSample2.setPan(0);
	soundSample1.setMultiPlay(false);
	/// #3
	soundSample3.load("eBass.wav");
	//soundSample3.setVolume(0.6f);           ///  volume is active in the OSC Leap section
	//soundSample3.play();                    ///  play is active in the key pressed function
	soundSample3.setLoop(false);
	soundSample3.setSpeed(1.0f);
	soundSample3.setPan(0);
	soundSample1.setMultiPlay(false);

	/// #4
	soundSample4.load("birdSong.mp3");
	//soundSample4.setVolume(1.0f);            ///  volume is active in the OSC Leap section
	//soundSample4.play();                    ///  play is active in the key pressed function
	soundSample4.setLoop(false);
	soundSample4.setSpeed(1.0f);
	soundSample4.setPan(0);
	soundSample1.setMultiPlay(false);


	/// IMAGE Load
	vignetteDotsImage.load("dotCircle.jpg");
	vignetteDotsImage2.load("dotCircleB.jpg");
	starsImage.load("stars.png");

	/// VIDEO Load
	//tunnelVid.load("tunnel1.mp4");
	//tunnelVid.play();
	//bImage.load("sn.jpg");
	

	/// SHADER Load
	drawStuffShader.load("drawStuff");


	////////////////////////////////
	///WEBCAM'S
	webCam.setDeviceID(1);            /// 0 - for rear, 1 for front camera.
	webCam.setDesiredFrameRate(60);
	//webCam.setup(640, 480);
	//color.allocate(webCam.getWidth(), webCam.getHeight());
	//gray.allocate(webCam.getWidth(), webCam.getHeight());
	//finder.findHaarObjects(webCam);

	////get back a list of devices.
	vector<ofVideoDevice> devices = webCam.listDevices();

	for (size_t i = 0; i < devices.size(); i++) {
		if (devices[i].bAvailable) {
			///log the device
			ofLogNotice() << devices[i].id << ": " << devices[i].deviceName;
		}
		else {
			///log the device and note it as unavailable
			ofLogNotice() << devices[i].id << ": " << devices[i].deviceName << " - unavailable ";
		}
	}

	


	

    ////////////////////////////////////////////////////////////
	///  MESH SHADER - main big mesh on which is the shader projected
	image.load("minta1.jpg");  //// "dotCircleB.jpg"    "stars.png"   "minta1.jpg"  "shell.png"
	image.resize(200 , 200);   /// this was low as (200, 200) /// Adjust the size of the shader mesh - add a slider
	webCam.setup(320, 240);
	//shaderMeshSizeD
	//+shaderMeshSizeW,
	//+shaderMeshSizeH,
	// + unitWidth
	// + unitHeight
	meshW = image.getWidth();
	meshH = image.getHeight();

	mesh.setMode(OF_PRIMITIVE_LINES);
	mesh.enableColors();
	/// We are going to be using indices this time
	mesh.enableIndices();

	float intensityThreshold = 140.0;    /// acts like an ISO - setting. mesh sensitivity - attach a slider
	int w = image.getWidth();            /// here I could add fluiditi to the size of the image - maybe noise
	int h = image.getHeight();       
	///int w = webCam.getWidth();    /// this set takes on the webcam proportions, whish will distort this mesh image
	///int h = webCam.getHeight();   
	for (int x = 0; x < w; x++) {
		for (int y = 0; y < h; y++) {
			ofColor c = image.getColor(x, y);
			///ofColor color = webCam.getPixels().getColor(i, j);
			///ofColor c = webCam.getPixels().getColor(x, y);
			float intensity = c.getLightness();
			if (intensity >= intensityThreshold) {
				float saturation = c.getSaturation();
				float z = ofMap(saturation, 0, 255, -100, 100);
				///!!!
				ofVec3f shadMeshPos(x*2, y*2, z*4);   /// it was low as *2 on each axes - this looks good, but!   - manipulating the mesh size // having problems to make it work!!!!
				//ofVec3f shadMeshPos(x*shaderMeshSizeW, y*shaderMeshSizeH, z*shaderMeshSizeD);   /// it was low as *2 on each axes    - manipulating the mesh size
				///ofVec3f pos(x, y, 0.0);
				mesh.addVertex(shadMeshPos);
				/// When addColor(...), the mesh will automatically convert
				/// the ofColor to an ofFloatColor
				mesh.addColor(c);

				/// And add this line to your existing for loop:
				/// It will create a ofVec3f with 3 random values, which
				/// will allow us to move the x, y and z coordinates of
				/// each vertex independently
				offsets.push_back(ofVec3f(ofRandom(0, 100000), ofRandom(0, 100000), ofRandom(0, 100000)));

			}
		}
	}

	/// Let's add some lines!
	float connectionDistance = 20;       /// acts like a hole maker in the net - or resolution  - dencity - bigger the number, heavier running
	int numVerts = mesh.getNumVertices();
	for (int a = 0; a < numVerts; ++a) {
		ofVec3f verta = mesh.getVertex(a);
		for (int b = a + 1; b < numVerts; ++b) {
			ofVec3f vertb = mesh.getVertex(b);
			float distance = verta.distance(vertb);
			if (distance <= connectionDistance) {
				// In OF_PRIMITIVE_LINES, every pair of vertices or indices will be
				// connected to form a line
				mesh.addIndex(a);
				mesh.addIndex(b);
			}
		}
	}


	///////////////////////////////////
	/// CONSTELLATION
	mesh1.setMode(OF_PRIMITIVE_LINES);



	/// GUI
	ofPushMatrix();
	//ofDisableArbTex();
	sliderGroup.setName("CONTROLLS");
	/// 2D Primitive circle resolution) Gui
	sliderGroup.add(intSlider.set("int slider", 3, 3, 64));
	sliderGroup.add(floatSlider.set("floatSlider", 1.0, 1.0, 256.0));
	/// Constellation Gui
	//sliderGroup.setName("Constellation");
	sliderGroup.add(uiPos1.set("uiPos1", 40, 0, 600));
	sliderGroup.add(uiPos2.set("uiPos2", 450, 0, 600));
	sliderGroup.add(uiPos3.set("uiPos3", 350, 0, 600));
	sliderGroup.add(uiPos4.set("uiPos4", 80, 0, 600));
	sliderGroup.add(uiPos5.set("uiPos5", 40, 0, 600));
	sliderGroup.add(uiPos6.set("uiPos6", 250, 0, 600));
	sliderGroup.add(uiPoints.set("uiPoints", 1, 0, 1));
	sliderGroup.add(uiDistance.set("uiDistance", 40, 0, 500));
	sliderGroup.add(uiAmount.set("uiAmount", 20, 0, 100));
	sliderGroup.add(frameSpeedMesh.set("frameSpeedMesh", 0.006, 0, 10));
	/// Shader Mesh Gui
	//sliderGroup.setName("Shader Mesh");
	sliderGroup.add(timeScale.set("timeScale", 9.0, 0.0, 20.0));
	sliderGroup.add(displacementScale.set("displacementScale", 0.75, 0.0, 10.0));

	/*sliderGroup.add(shaderMeshSizeW.set("shaderMeshSizeW", 2, 0, 10)); /// this is not working yet
	sliderGroup.add(shaderMeshSizeH.set("shaderMeshSizeH", 2, 0, 10));
	sliderGroup.add(shaderMeshSizeD.set("shaderMeshSizeD", 4, 0, 10));*/

	/// Sound Gui
	sliderGroup.add(volume.set("volume", 0.5, 0.0, 1.0));
	sliderGroup.add(decay.set("decay", 0.5, 0.0, 1.0));

	/// Light - Gui
	sliderGroup.add(uiPosition.set("position",ofVec3f(0,0,0),ofVec3f(-10000,-10000,-10000),ofVec3f(10000,10000,10000)));

	/// Rotation - Gui
	sliderGroup.add(uiRotationX.set("rotationX", 0, 0, 5));
	sliderGroup.add(uiRotationY.set("rotationY", 0, 0, 5));
	sliderGroup.add(uiRotationZ.set("rotationZ", 0, 0, 5));

	/// Width scale - used as a unit
	sliderGroup.add(unitWidth.set("Width", 40, 0, 4000));
	/// Height scale - used as a unit
	sliderGroup.add(unitHeight.set("Height", 25, 0, 2500));
	/// Height scale - used as a unit
	sliderGroup.add(unitDepth.set("Depth", 50, 0, 500));

	gui.setup(sliderGroup);

	drawCommands = false;
	//ofEnableArbTex();
	ofPopMatrix();



	/// FFT
	fft = new float[128];
	for (int i = 0; i < 128; i++) {
		fft[i] = 0;
	}
	bands = 64;



	/// CAMERA Observer
	//camera.disableMouseInput();
	//camera.setupPerspective();
	camera.setPosition(0.0, 0.0, 540.0);
	camera.setTarget(vec3(0.0, 4.0, 0.0));
	camera.setFov(32.0);
	//camera.setNearClip(0.05f);
	//camera.setFarClip(10000.0f);
	camera.setAutoDistance(false);




	///MATERIAL - Initialize materials
	whiteMaterial.setDiffuseColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	whiteMaterial.setSpecularColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	whiteMaterial.setAmbientColor(ofFloatColor(1.0f, 0.2f, 0.1f));
	whiteMaterial.setShininess(100.0);

	redMaterial.setDiffuseColor(ofFloatColor(1.0f, 0.2f, 0.1f));
	redMaterial.setSpecularColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	redMaterial.setAmbientColor(ofFloatColor(1.0f, 0.2f, 0.1f));
	redMaterial.setShininess(100.0);

	greenMaterial.setDiffuseColor(ofFloatColor(0.2f, 1.0f, 0.2f));
	greenMaterial.setSpecularColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	greenMaterial.setAmbientColor(ofFloatColor(0.2f, 1.0f, 0.2f));
	greenMaterial.setShininess(50.0);

	blueMaterial.setDiffuseColor(ofFloatColor(0.1f, 0.2f, 1.0f));
	blueMaterial.setSpecularColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	blueMaterial.setAmbientColor(ofFloatColor(0.1f, 0.2f, 1.0f));
	blueMaterial.setShininess(10.0);

	///LIGHT - Initialize lights
	pointLight.setDiffuseColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	pointLight.setSpecularColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	pointLight.setPosition(vec3(410.0f, 410.0f, 600.0f));

	ofSetGlobalAmbientColor(ofFloatColor(0.1f, 0.1f, 0.1f));

	pointLight.enable();

	
	// Initialize mesh primitives
	//mySphere.setRadius(1.0f);

	/*myCone.setRadius(1.2f);
	myCone.setHeight(2.0f);
	myCone.setResolutionRadius(32);*/

	myBox.setWidth(9.0f);
	myBox.setHeight(9.0f);
	myBox.setDepth(9.0f);


	/// This plane is to be used for video projection
	//myPlane.setWidth(10.0f);
	//myPlane.setHeight(5.0f);


	//cloudVid.load("FL1.mov");
	//themeSong.play();
	//cloudVid.setVolume(0.0f);
	//cloudVid.setLoopState(OF_LOOP_PALINDROME);




	// Initialize textures
	ofEnableNormalizedTexCoords();
}




/// UPDATE   --------------------------------------------------------------
void ofApp::update(){

	plusPlus += 1;    /// this global variable is acting as a ++ 
	minusMinus -= 1; /// this global variable is acting as a --

	/// LIGHT
	pointLight.setPosition(uiPosition->x, uiPosition->y, uiPosition->z);   /// position of the light source - adjustable on the XYZ axis via slider
	//light.setPosition(ofGetWidth()*.5, ofGetHeight()*.5, ofRandom(500, 1500));                  /// repetitive light flickering 
	//light.setPosition(ofGetWidth()*.5, ofGetHeight()*.5, 2000 * sin(ofGetElapsedTimef() / 2));    ///slower oscillation of the light


	///VIDEO
	//tunnelVid.update();

	
	/// WEBCAM
	webCam.update();


	///VIDEO
	cloudVid.update();




	///SOUND
	ofSoundUpdate();
	soundSample0.setVolume(volume);
	soundSpectrum = ofSoundGetSpectrum(bands);
	for (int i = 0; i < bands; i++) {
		fft[i] *= decay;
		if (fft[i] < soundSpectrum[i]) {
			fft[i] = soundSpectrum[i];
		}
	}

	 



	/// SHADER MESH
	int numVerts = mesh.getNumVertices();
	for (int i = 0; i < numVerts; ++i) {
		ofVec3f vert = mesh.getVertex(i);

		float time = ofGetElapsedTimef();
		//float timeScale = 9.0;              /// increases or decreases the movement in time of the mesh - frequency -  added a slider for it
		//float displacementScale = 0.75;    /// increases or decreases the movement in space of the mesh - amplitude -  added a slider for it
		ofVec3f timeOffsets = offsets[i];

		/// A typical design pattern for using Perlin noise uses a couple parameters:
		/// ofSignedNoise(time*timeScale+timeOffset)*displacementScale
		///     ofSignedNoise(time) gives us noise values that change smoothly over
		///         time
		///     ofSignedNoise(time*timeScale) allows us to control the smoothness of
		///         our noise (smaller timeScale, smoother values)
		///     ofSignedNoise(time+timeOffset) allows us to use the same Perlin noise
		///         function to control multiple things and have them look as if they
		///         are moving independently
		///     ofSignedNoise(time)*displacementScale allows us to change the bounds
		///         of the noise from [-1, 1] to whatever we want
		/// Combine all of those parameters together, and you've got some nice
		/// control over your noise

		//vert.x += (ofSignedNoise(time*timeScale + timeOffsets.x)) * displacementScale; /////fft[i] *
		//vert.y += (ofSignedNoise(time*timeScale + timeOffsets.y)) * displacementScale;
		//vert.z += (ofSignedNoise(time*timeScale + timeOffsets.z)) * displacementScale;
		//vert.x += (ofSignedNoise(time*timeScale + timeOffsets.x)) / (fft[i]); /////fft[i] *
		//vert.y += (ofSignedNoise(time*timeScale + timeOffsets.y)) / (fft[i]);
		//vert.z += (ofSignedNoise(time*timeScale + timeOffsets.z)) / (fft[i]);


		/// This dislocates the mesh on z axes. nice, but not yet controllable
		//vert.x += (ofSignedNoise(time*timeScale + timeOffsets.x)) * displacementScale; /////fft[i] *
		//vert.y += (ofSignedNoise(time*timeScale + timeOffsets.y)) * displacementScale;
		//vert.z = (fft[i])/64;

		/// This one is the initial working one
		vert.x += (ofSignedNoise(time*timeScale + timeOffsets.x)) * displacementScale; /////fft[i] *
		vert.y += (ofSignedNoise(time*timeScale + timeOffsets.y)) * displacementScale;
		vert.z += (ofSignedNoise(time*timeScale + timeOffsets.z)) * displacementScale;

		mesh.setVertex(i, vert);
	}


	/// OSC Leap Motion
	/// This OSC is connected to Leap Motion through Geco app
	/// 
	while (oscReceiver.hasWaitingMessages()) {
		ofxOscMessage msg;
		oscReceiver.getNextMessage(msg);



		/// OSC LEAP Up-Down reading with Closed/Open LEFT HAND
		if (msg.getAddress() == "/left/open/movy" || msg.getAddress() == "/left/closed/movy") {

			uiDistance = msg.getArgAsFloat(0);
			uiDistance = ofMap(uiDistance, 0, 1, 0, 600);

			float vol;
			vol = msg.getArgAsFloat(0);
			vol = ofMap(vol, 0, 1, 0, 1);     /// not nesessary 
			soundSample1.setVolume(vol);
			soundSample2.setVolume(vol);
			soundSample3.setVolume(vol);
			soundSample4.setVolume(vol);

		}


		/// OSC LEAP Left - Right reading with Closed/Open LEFT HAND
		if (msg.getAddress() == "/left/open/movx" || msg.getAddress() == "/left/closed/movx") {

			/// unitWidth - slider
			unitWidth = msg.getArgAsFloat(0);
			unitWidth = ofMap(unitWidth, 0, 1, 0, 4000);

			/// unitHeight - slider
			unitHeight = msg.getArgAsFloat(0);
			unitHeight = ofMap(unitHeight, 0, 1, 0, 2000);
		}

		/// this one is for soom, but not sure how yet
		/// OSC LEAP Backward - Forward reading with Closed/Open LEFT HAND
		if (msg.getAddress() == "/left/open/movz" || msg.getAddress() == "/left/closed/movz") {

			//reserved for zoom

		}

		/// ROTATION COMMANDS
		/// OSC LEAP Pitch Inclination reading with Closed/Open LEFT HAND
		if (msg.getAddress() == "/left/closed/pitch" || msg.getAddress() == "/left/open/pitch") {

			/// uiRotationZ - slider
			uiRotationX = msg.getArgAsFloat(0);
			uiRotationX = ofMap(uiRotationX, 0, 1, 0, 5);
		}

		/// OSC LEAP Yaw Inclination reading with Closed/Open LEFT HAND
		if (msg.getAddress() == "/left/open/yaw" || msg.getAddress() == "/left/closed/yaw") {

			/// uiRotationZ - slider
			uiRotationY = msg.getArgAsFloat(0);
			uiRotationY = ofMap(uiRotationY, 0, 1, 0, 5);
		}

		/// OSC LEAP Roll Inclination reading with Closed/Open LEFT HAND
		if (msg.getAddress() == "/left / closed / roll" || msg.getAddress() == "/left/open/roll") {

			/// uiRotationZ - slider
			uiRotationZ = msg.getArgAsFloat(0);
			uiRotationZ = ofMap(uiRotationZ, 0, 1, 0, 5);
		}


		//if (msg.getAddress() == "/left/open/present" || "/left/closed/present" || "/right/open/present" || "/right/closed/present") {  
		// } 

	}/// END OF OSC
	




	/// CONSTELLATION - Mesh
	ofSeedRandom(30);
	mesh1.clear();
	for (int i = 0; i < uiAmount; i++) {
		//float frameSpeedMesh = 0.006; // *sin(ofGetElapsedTimef() / 4);   /// it is implemented in slider 
		position = ofVec3f(
			//ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * 0.006), 0, 1, uiPos1 , uiPos2),
			ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * frameSpeedMesh), 0, 1, uiPos1 + 2 * sin(ofGetElapsedTimef() / 4), uiPos2),
			//ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * 0.006), 0, 1, ofRandom(uiPos1 + ofGetMouseX(), uiPos1 - ofGetMouseX()), uiPos2 + ofGetMouseX()),
			ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * frameSpeedMesh), 0, 1, uiPos3, uiPos4),
			ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * frameSpeedMesh), 0, 1, uiPos5, uiPos6)
		);
		mesh1.addVertex(position);
		//mesh1.addColor(ofColor(255)); /// this is where I change the colour of the sole mesh - If this is on, it overrides all the meshes in this MESH1!
	}

	for (int i = 0; i < mesh1.getVertices().size(); i++) {
		auto position = mesh1.getVertex(i);
		for (int j = 0; j < mesh1.getVertices().size(); j++) {
			auto jdistance = mesh1.getVertex(j);
			auto distance = glm::distance(position, jdistance);

			if (distance < uiDistance) {
				mesh1.addIndex(i);
				mesh1.addIndex(j);
			}
		}
	}


	/// GUI
	ofSetCircleResolution(intSlider);
	
}

//--------------------------------------------------------------
void ofApp::draw(){

	/// BACKGROUND
	//ofSetBackgroundColor(0); ///For examination it is best the white
	ofSetBackgroundColor(2, 1, 10); ///For examination it is best the white
	//ofColor centerColor = ofColor(255, 255, 255, ofRandom(255,200));
	//ofColor centerColor = ofColor(56, 0, 11);    
	//ofColor edgeColor(0, 0, 0, ofRandom(255, 200));
	//ofBackgroundGradient(centerColor, edgeColor, OF_GRADIENT_CIRCULAR);  /// this gradient mode gives a certain mood to the work
	/// Experimental
	//ofBackground(ofRandom(255, 0));
	//ofBackground(255 * sin(ofGetElapsedTimef()/200));



	///GUI - needs to be here at the top wrapped in ofDisableDepthTest()
	ofPushMatrix();
	ofDisableDepthTest();
	if (drawCommands == true) {
		gui.draw();
	}
	ofPopMatrix();

	ofEnableDepthTest();

	/// LIGHT - START
	ofEnableLighting();

	/// CAMERA - START
	camera.begin();


	/// ROTATION - of the whole draw
	ofRotateXDeg(uiRotationX * plusPlus);
	ofRotateYDeg(uiRotationY * plusPlus);
	ofRotateZDeg(uiRotationZ * plusPlus);



	whiteMaterial.begin();
	starsImage.bind();
	ofPushMatrix();
	//ofTranslate(vec3(-5.0f, -5.0f, 5.0f));
	ofRotateYDeg(-90.0f);
	ofRotateZDeg(180.0f);
	//ofSetColor(0, 0, 0, 30);
	myBox.draw();
	ofPopMatrix();
	starsImage.unbind();
	whiteMaterial.end();


	ofDrawGrid(1, 5, false, true, true, true);

	
	

	
	/// CONSTELLATION
	ofPushMatrix();
	ofTranslate(-300, -500, -400);  ///100*fft[i]
	whiteMaterial.begin();
	if (uiPoints == 1) {
		for (int i = 0; i < mesh1.getVertices().size(); i++) {


			///Motion for the mesh as a group - better in the for loop, so I can access the (i)
			



			/// mesh1 #1      ////////////////
			ofPushMatrix();  /// all mesh1 #1

			ofTranslate(mesh1.getVertex(i) / 3);

			ofPushMatrix();  /// myBox
			starsImage.bind();
			//ofRotateXDeg(frameSpeedMesh*2);
			//ofRotateYDeg(frameSpeedMesh*2);
			//ofRotateZDeg(frameSpeedMesh*2);
			ofTranslate(mesh1.getVertex(i));
			ofScale(3* fft[i], 3 * fft[i], 3 * fft[i]);
			myBox.draw();
			starsImage.unbind();
			ofPopMatrix();   /// myBox

			mesh1.addColor(ofFloatColor(255.0, 0.0, 0.0, ofRandom(10, 200)));  // add a color at that vertex
			mesh1.draw();

			ofPopMatrix();  /// all mesh1 #1



			/// mesh1 #2     ///////////////////////////////////
			ofPushMatrix(); /// all mesh1 #2

			ofTranslate(mesh1.getVertex(i) / 4);

			ofPushMatrix();  /// myBox
			starsImage.bind();
			//ofRotateXDeg(frameSpeedMesh*2);
			//ofRotateYDeg(frameSpeedMesh*2);
			//ofRotateZDeg(frameSpeedMesh*2);
			ofTranslate(mesh1.getVertex(i));
			ofScale(3 * fft[i], 3 * fft[i], 3 * fft[i]);
			myBox.draw();
			starsImage.unbind();
			ofPopMatrix();  /// myBox

			ofTranslate(30 * fft[i], 30 * fft[i], 30 * fft[i]);
			mesh1.addColor(ofFloatColor(255.0, 0.0, 0.0, ofRandom(10, 200)));  // add a color at that vertex
			mesh1.draw();

			ofPopMatrix();  /// all mesh1 #2




			/// mesh1 #3  ////////////////////////////////
			ofPushMatrix();      /// all mesh1 #3

			ofPushMatrix();     /// myBox
			starsImage.bind();
			//ofRotateXDeg(frameSpeedMesh*2);
			//ofRotateYDeg(frameSpeedMesh*2);
			//ofRotateZDeg(frameSpeedMesh*2);
			ofTranslate(mesh1.getVertex(i));
			ofScale(3 * fft[i], 3 * fft[i], 3 * fft[i]);
			myBox.draw();
			starsImage.unbind();
			ofPopMatrix();      /// myBox

			ofTranslate(30 * fft[i], 30 * fft[i], 30 * fft[i]);
			mesh1.addColor(ofFloatColor(255.0, 0.0, 0.0, ofRandom(10, 200)));  // add a color at that vertex
			mesh1.draw();

			ofPopMatrix();   /// all mesh1 #3


			
		}
	}
	whiteMaterial.end();
	ofPopMatrix();


	//tunnelVid.draw(0,0,100,100);

	

	/// SHADER START
	ofPushMatrix();
	ofTranslate(0 - meshW, 0 - meshH, 0);   /// this is necessary because the shader's mesh 000 is from bottom left
	drawStuffShader.begin();

	///UNIFORM TEXTURES
	drawStuffShader.setUniformTexture("imageTexture", webCam.getTexture(), 1);               /// webCam
	drawStuffShader.setUniformTexture("imageTexture1", vignetteDotsImage.getTexture(), 2);   /// vignette dots
	drawStuffShader.setUniformTexture("imageTexture2", starsImage.getTexture(), 3);          /// stars 
	//drawStuffShader.setUniformTexture("imageTexture3_vid", tunnelVid.getTexture(), 5);     /// video
	
	/// UNIFORM VARIABLES
	drawStuffShader.setUniform1f("u_time", ofGetElapsedTimef());
	drawStuffShader.setUniform2f("mousePos", vec2(ofGetMouseX(), ofGetHeight() - ofGetMouseY()));
	drawStuffShader.setUniform2f("rectangleSize", vec2(unitWidth, unitHeight));   ////(1080, 960)  (640, 480)
	drawStuffShader.setUniform2f("u_resolution", webCam.getWidth(), webCam.getHeight());

	
	ofPushMatrix();
	//ofScale(1.5);
	//ofTranslate(0, 0,0);
	//ofRotateDeg(2 * sin(ofGetElapsedTimef() / 2) + (2 * cos(ofGetElapsedTimef() / 2)));
	mesh.draw();
	ofPopMatrix();
	
	ofDrawGrid(2.0, 10, false, false, true, false);
	

	/// SHADER END
	drawStuffShader.end();
	ofPopMatrix();

	
	
	/// CAMERA - END
	camera.end();

	///LIGHT - END
    ofDisableLighting();

	ofDisableDepthTest();
}




//--------------------------------------------------------------
void ofApp::keyPressed(int key){

	/// FULL SCREEN
	if (key == 'f') {
		ofToggleFullscreen();
	}

	/// GUI - draw commands
	if (key == 'h') {
		drawCommands = true;
		ofShowCursor();
	}
	

	

	/// SOUND
	if (key == '1') {
		
		soundSample1.play();
	}

	if (key == '2') {
		
		soundSample2.play();
	}

	if (key == '3') {
		
		soundSample3.play();
	}

	if (key == '4') {
		
		soundSample4.play();
	}


	/*/// CAMERA COMMANDS
	if (key == 's') {       /// zoom out
		cam.dolly(20);
	}
	if (key == 'w') {       /// zoom in
		cam.dolly(-20);
	}
	if (key == 'a') {       /// pan left
		cam.panDeg(-10);
	}
	if (key == 'd') {       /// pan right
		cam.panDeg(10);
	}
	if (key == 'q') {       /// tilt down
		cam.tiltDeg(-10);
	}
	if (key == 'e') {       /// tilt up
		cam.tiltDeg(10);
	}*/

	

}



//--------------------------------------------------------------
void ofApp::keyReleased(int key){

	///GUI - Hide commands
	if (key == 'h') {
	drawCommands = false;
	ofHideCursor();
	}

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}


////--------------------------------------------------------------
//void ofApp::dragEvent(ofDragInfo dragInfo){ 
//	vector< string > fileList = dragInfo.files;
//	load(fileList[0]);
//}


//ofRectangle ofApp::getBarRectangle() const
//{
//	return ofRectangle(BarInset, ofGetWindowHeight() - BarInset - BarHeight, ofGetWindowWidth() - (2 * BarInset), BarHeight);
//}