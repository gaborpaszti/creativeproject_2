#pragma once

#include "ofMain.h"

#include "ofxOsc.h"
#include "ofxMaxim.h"
#include "ofxGui.h"
#include "ofxHapPlayer.h"

#define PORT 12345          /// port for the leap motion osc - Geco
#define HOST "localhost"


using namespace glm;  /// without this shader wont work - but with this particle system does not work yet



class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		/// OSC 
		ofxOscReceiver oscReceiver;
		float timeReceived;
		//ofxOscSender oscSender;
		//float timeSent;
		float oscx = 0.0;
		float oscy = 0.0;

		/// GUI - CONSTELLATION
		bool drawCommands;

		ofxPanel gui;
		ofParameterGroup sliderGroup;
		ofParameter<int> intSlider;
		ofParameter<float> floatSlider;

		ofParameter <int> uiAmount;
		ofParameter <float> uiPos1;
		ofParameter <float> uiPos2;
		ofParameter <float> uiPos3;
		ofParameter <float> uiPos4;
		ofParameter <float> uiPos5;
		ofParameter <float> uiPos6;
		ofParameter <float> uiDistance;
		ofParameter <bool> uiPoints;



		/// Parameters for shader mesh
		ofParameter <float> timeScale; //= 9.0;              /// increases or decreases the movement in time of the mesh - frequency - to add a slider for it
		ofParameter <float> displacementScale; // = 0.75;    /// increases or decreases the movement in space of the mesh - amplitude -  to add a slider for it

		/// not working
		//ofParameter <float> shaderMeshSizeW; /// shader mesh width
		//ofParameter <float> shaderMeshSizeH; /// shader mesh heigth
		//ofParameter <float> shaderMeshSizeD; /// shader mesh depth									   
											   
	    
        /// Speed of the  CONSTELLATION mesh
		ofParameter <float> frameSpeedMesh;

		/// Sound gui - fft
		ofParameter <float> volume;
		ofParameter <float> decay;    /// small value is erratic, bigger is slower

		/// Light gui
		ofParameter <ofVec3f> uiPosition;    /// Light source position

		/// Rotation gui
		ofParameter <float> uiRotationX;    /// Rotation of the whole draw
		ofParameter <float> uiRotationY;    /// Rotation of the whole draw
		ofParameter <float> uiRotationZ;    /// Rotation of the whole draw


		/// Dirrection units
		ofParameter <float> unitWidth;     ///  
		ofParameter <float> unitHeight;    /// 
		ofParameter <float> unitDepth;     /// 



		/// CONSTELLATION
		ofVec3f position; /// position of vectors in constellation

		///MESH for CONSTELLATION
		ofMesh mesh1;

		/// SHADER
		ofShader drawStuffShader;  /// shader 

		/// SOUND
		ofSoundPlayer soundSample0;  /// Main theme on repeat

		ofSoundPlayer soundSample1;  /// 
		ofSoundPlayer soundSample2;
		ofSoundPlayer soundSample3;
		ofSoundPlayer soundSample4;

		ofxHapPlayer themeSong;  /// the main song - plays in loop

		/// FFT
		float *fft;
		float *soundSpectrum;
		int bands;
		
		
		/// WEB-CAM
		ofVideoGrabber webCam;    /// webcam

		/// CAMERA
		ofEasyCam camera;         /// wiever camera

		/// VIDEO
		//ofVideoPlayer cloudVid;  /// video player 
		ofxHapPlayer cloudVid;


		/// IMAGE - all images needed for the code are listed here
		ofImage image;

		ofImage starsImage;
		ofImage vignetteDotsImage;
		ofImage vignetteDotsImage2;

		ofImage bImage;               /// this image is an artwork , might not needed

		/// MESH - SHADER
		ofMesh mesh;                /// mesh  for the shader 
		vector<ofVec3f> offsets;   /// this is for movement on Z axes with noise
		float meshW;              /// Widht of the mesh
		float meshH;             /// Height of the mesh 


		/// 3D Primitives 
		ofSpherePrimitive mySphere;
		ofConePrimitive myCone;
		ofPlanePrimitive myPlane;
		ofBoxPrimitive myBox;

		/// Materials 
		ofMaterial redMaterial;
		ofMaterial greenMaterial;
		ofMaterial blueMaterial;
		ofMaterial whiteMaterial;

		/// LIGHT
		ofLight light;            /// light 
		ofLight pointLight;       /// light 


		/*/// UNITS - variables to which the rest mesures
		float unitHeight;
		float unitWidth; */

		float plusPlus; /// this global variable is acting as a ++ 
		float minusMinus; /// this global variable is acting as a --
};

