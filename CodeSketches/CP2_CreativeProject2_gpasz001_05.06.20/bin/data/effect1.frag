#version 150
precision highp float;
#define PI 3.1415926538


out vec4 fragColor;

uniform vec2 u_resolution;
uniform vec2 u_time;
uniform vec2 mousePos;

uniform vec2 windowSize;
uniform vec2 rectangleSize;
uniform vec2 rectangleSize1;

uniform sampler2D imageTexture;           ///webcam
uniform sampler2D imageTexture1;          ///dotCircle
uniform sampler2D imageTexture2;          ///stars


/// These are passed in from OF programmable renderer
uniform mat4 modelViewMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 textureMatrix;
uniform mat4 modelViewProjectionMatrix;


uniform float uScale;      /// For imperfect, isotropic anti-aliasing in
uniform float uYrot;      /// absence of dFdx() and dFdy() functions
float frequency = 40.0;  /// Needed globally for lame version of aastep()





/// TEST IS IT INSIDE THE RECTANGLE
////////////////////////////////////////////////////////////////////////////
bool testInsideRectangle(vec2 pixelPos, vec2 rectPos, vec2 rectSize){

		    // Tests if a pixel is inside a rectangle centred on rextPos
			if (pixelPos.x > rectPos.x - 0.5 * rectSize.x &&
				pixelPos.x < rectPos.x + 0.5 * rectSize.x &&
				pixelPos.y > rectPos.y - 0.5 * rectSize.y &&
				pixelPos.y < rectPos.y + 0.5 * rectSize.y) {
			
				return true;
			}
			else
			{
				return false;
			}

}


/// Position inside the rectangle
vec2 posInsideRectangle(vec2 pixelPos, vec2 rectPos, vec2 rectSize) {
    return (pixelPos - rectPos + 0.5 * rectSize) / rectSize;
}




/// HALFTONE - Effect
/// the code for halftone effect is salvaged from :
///http://weber.itn.liu.se/~stegu/webglshadertutorial/shadertutorial.html
/////////////////////////////////////////////////////////////
/// Description : Array- and textureless GLSL 2D simplex noise.
/// Author : Ian McEwan, Ashima Arts. Version: 20110822
/// Copyright (C) 2011 Ashima Arts. All rights reserved.
/// Distributed under the MIT License. See LICENSE file.
/// https://github.com/ashima/webgl-noise

float aastep(float threshold, float value) {
  float afwidth = 0.7 * length(vec2(dFdx(value), dFdy(value)));
  return smoothstep(threshold-afwidth, threshold+afwidth, value);

}

 
vec2 mod289(vec2 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
vec3 mod289(vec3 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
vec3 permute(vec3 x) { return mod289((( x * 34.0) + 1.0) * x); }
 
float snoise(vec2 v) {
	  const vec4 C = vec4(0.211324865405187,  /// (3.0-sqrt(3.0))/6.0
						  0.366025403784439,  /// 0.5*(sqrt(3.0)-1.0)
						 -0.577350269189626,  /// -1.0 + 2.0 * C.x
						  0.024390243902439); /// 1.0 / 41.0
	  /// First corner
	  vec2 i = floor(v + dot(v, C.yy) );
	  vec2 x0 = v - i + dot(i, C.xx);
	  /// Other corners
	  vec2 i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
	  vec4 x12 = x0.xyxy + C.xxzz;
	  x12.xy -= i1;
	  /// Permutations
	  i = mod289(i); // Avoid truncation effects in permutation
	  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
							   + i.x + vec3(0.0, i1.x, 1.0 ));
	  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy),
							  dot(x12.zw,x12.zw)), 0.0);
	  m = m*m; m = m*m;
	  /// Gradients
	  vec3 x = 2.0 * fract(p * C.www) - 1.0;
	  vec3 h = abs(x) - 0.5;
	  vec3 a0 = x - floor(x + 0.5);
	  /// Normalise gradients implicitly by scaling m
	  m *= 1.792843 - 0.853735 * ( a0*a0 + h*h );
	  /// Compute final noise value at P
	  vec3 g;
	  g.x = a0.x * x0.x + h.x * x0.y;
	  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
	  return 130.0 * dot(m, g);
}



/// MAIN  
void main (void){
        
		float circleRes = 12.22;

        vec2 pixelPos = gl_FragCoord.xy; 

		if (testInsideRectangle(pixelPos, mousePos, rectangleSize)) {
		    vec2 texturePos = posInsideRectangle(pixelPos, mousePos, rectangleSize);

			texturePos.y = 1.0 - texturePos.y;  /// invering the screen - in this case putting it back to normal
		   
			/// TEXTURE COLORS 
			vec3 textureColor = texture(imageTexture, texturePos).rgb;   /// webCam
			vec3 textureColor1 = texture(imageTexture1, texturePos).rgb; /// vignetteImage
			vec3 textureColor2 = texture(imageTexture2, texturePos).rgb; /// starImage
			
			
			/// DOT PIXEL
			float n = 0.1 * snoise(pixelPos * 200.0); /// Fractal noise
			n += 0.05*snoise(texturePos * 400.0);
			n += 0.025*snoise(texturePos * 800.0);
			
			vec3 white = vec3(n*0.5 + 0.98);
			vec3 black = vec3(n + 0.1);

			/// Perform a rough RGB-to-CMYK conversion
			vec4 cmyk;
			cmyk.xyz = 1.0 - textureColor;
			cmyk.w = min(cmyk.x, min(cmyk.y, cmyk.z)); // Create K
			cmyk.xyz -= cmyk.w; // Subtract K equivalent from CMY

			/// Distance to nearest point in a grid of
			/// (frequency x frequency) points over the unit square
			vec2 Kst = frequency*mat2(0.707, -0.707, 0.707, 0.707) * (texturePos * circleRes);
			vec2 Kuv = 2.0*fract(Kst)-1.0;
			float k = aastep(0.0, sqrt(cmyk.w)-length(Kuv)+n);
			vec2 Cst = frequency*mat2(0.966, -0.259, 0.259, 0.966) * (texturePos * circleRes);
			vec2 Cuv = 2.0*fract(Cst)-1.0;
			float c = aastep(0.0, sqrt(cmyk.x)-length(Cuv)+n);
			vec2 Mst = frequency * mat2(0.966, 0.259, -0.259, 0.966) * (texturePos * circleRes);
			vec2 Muv = 2.0*fract(Mst)-1.0;
			float m = aastep(0.0, sqrt(cmyk.y)-length(Muv)+n);
			vec2 Yst = frequency * (texturePos * circleRes); // 0 deg
			vec2 Yuv = 2.0*fract(Yst)-1.0;
			float y = aastep(0.0, sqrt(cmyk.z)-length(Yuv)+n);

			vec3 rgbscreen = 1.0 - 0.9 * vec3(c,m,y) + n;
            rgbscreen = mix(rgbscreen, black, 0.85 * k + 0.3 * n);
			
			float afwidth = 2.0 * frequency * max(length(dFdx(texturePos)), length(dFdy(texturePos)));
			float blend = smoothstep(0.7, 1.4, afwidth); 
			
			

			fragColor = vec4(mix(rgbscreen, textureColor2, textureColor1 ), 1.0);   /// The final output
	
		} else {

			fragColor = vec4(0.1, 0, 1, 1);    /// Changing this will determine the color of the mesh, when the shader is not covering it.

		}

}


