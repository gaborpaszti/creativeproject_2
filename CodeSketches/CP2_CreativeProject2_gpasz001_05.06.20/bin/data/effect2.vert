#version 150

out vec2 v_texcoord;

in vec4 position;
in vec4 color;
in vec4 normal;
in vec2 texcoord;


/// Uniforms for movement
uniform float time;
uniform float amplitude;
uniform float freqTime;
uniform float freqSpace;
uniform float keyP;


/// These are passed in from OF programmable renderer
uniform mat4 modelViewMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 textureMatrix;
uniform mat4 modelViewProjectionMatrix;

void main (void){

    v_texcoord = texcoord;


    vec4 posModelSpace = position;
	
		posModelSpace.y += amplitude * sin(freqTime * time/2 + freqSpace * posModelSpace.y);
	

	vec4 posWorldSpace = modelMatrix * posModelSpace;
	
		posWorldSpace.y += amplitude * sin(freqTime * time/2 + freqSpace * posWorldSpace.y);
	

	vec4 posViewSpace = viewMatrix * posWorldSpace;
	
		posViewSpace.y += amplitude * sin(freqTime * time/2 + freqSpace * posViewSpace.y);
	

	vec4 posProjectionSpace = projectionMatrix * posViewSpace;
	
		posProjectionSpace.y += amplitude * sin(freqTime * time/2 + freqSpace * posProjectionSpace.y);
	

	posProjectionSpace /= posProjectionSpace.w;
	gl_Position = posProjectionSpace; 


}
