#pragma once

#include "ofMain.h"

using namespace glm;

#define AMOUNT 3

class graphicEffect2 {

public:
	void setup();
	void update();
	void draw();
	void keyPressed(int key);

	/// Function for custom mesh
	ofMesh createEllipseWithTextureCoords(float xRad, float yRad, int steps);

	/// Meshes
	ofMesh ellipseMesh;

	/// SHADER
	ofShader effect2Shader;        /// shader 

	/// MESH - SHADER
	ofMesh mesh2;                  /// mesh2 for effect 2 
	
	/// VIDEO
	ofVideoPlayer cloudVid;       /// video player 

	/// Noise movement 
	vector<float> speedX;
	vector<float> speedY;
	vector<float> speedZ;

	/// Mesh Moevemt Variables
	float time;
	float amplitude;
	float freqTime;
	float freqSpace;
	float rot;

	float hSize;
	float wSize;
	int shapeRes;
	float shapeRot;
	
};
