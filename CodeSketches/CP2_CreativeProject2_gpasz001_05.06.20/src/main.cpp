#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){
	
	ofGLWindowSettings settings;
	settings.setGLVersion(3, 2);
	settings.setSize(1280, 720);
	ofCreateWindow(settings);
	ofRunApp(new ofApp());

}




/* 
NOTES:

This code is composed by Gabor Paszti (gpasz001@gold.ac.uk)
The code is a tool for an art installation, which provides graphics and sound. 

Controllers used for this project are: mouse, standalone trackpad, Leap Motion.

Key codes controls:

"0", "1", "2", "3", "4"   = Loading different sounds
"h" = Hides - unhides the control pannel (gui pnel)
"f" = Activates ful screen

Leap Motion gesture controls:

    active gestures:  "/left/open/movx" and "/left/closed/movx"        /// activating variables connected to of width
	                  "/left/open/movy" and "/left/closed/movy"        /// triggers mesh behaviour and some of the sound properties
					  "/left/open/movz" and "/left/closed/movz"        /// "camera" zoom in and out 
					  "/left/closed/pitch" and "/left/open/pitch"      /// rotation on "x" axis
					  "/left/open/yaw" and "/left/closed/yaw"	       /// rotation on "y" axis
					  "/left / closed / roll" and "/left/open/roll"    /// rotation on "z" axis

Trackpad gestures are responding to a camera controls (easyCam)

*/