#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	finder.setup("haarcascade_frontalface_default.xml");
	finder.setPreset(ofxCv::ObjectFinder::Fast);
	webCam.setDeviceID(1);            /// 0 - for rear, 1 for front camera.
	webCam.setDesiredFrameRate(60);
	webCam.setup(640, 480);
}

//--------------------------------------------------------------
void ofApp::update(){
	webCam.update();
	if (webCam.isFrameNew()) {
		finder.update(webCam);
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	webCam.draw(0, 0);

	ofPushMatrix();
	ofSetColor(255, 0, 0);		
	finder.draw();
	ofPopMatrix();
	ofSetColor(255);

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
