#include "demoParticle.h"
#include "ofApp.h"

//------------------------------------------------------------------
//demoParticle::demoParticle(ofEasyCam *cam){
demoParticle::demoParticle() {

	attractPoints = NULL;
	//this->cam = cam;

	//m = Mesh(30, 30);   
	
}

//------------------------------------------------------------------
void demoParticle::setMode(particleMode newMode){

	mode = newMode;
}

//------------------------------------------------------------------
void demoParticle::setAttractPoints( vector <ofPoint> * attract ){

	attractPoints = attract;
}

//void demoParticle::setTexture(const ofTexture& tex){
//
//	//box.setTexture(tex);
//}

//------------------------------------------------------------------
void demoParticle::reset(){
	//the unique val allows us to set properties slightly differently for each particle
	uniqueVal = ofRandom(-10000, 10000);
	
	pos.x = ofRandomWidth();
	pos.y = ofRandomHeight();
	pos.z = 0;

	
	const float MaxVel = 3.9;
	vel.x = ofRandom(-MaxVel, MaxVel);
	vel.y = ofRandom(-MaxVel, MaxVel);
	vel.z = 0;
	
	frc   = ofPoint(0,0,0);
	
	scale = ofRandom(0.5, 1.0);
	
	if( mode == PARTICLE_MODE_NOISE ){
		drag  = ofRandom(0.97, 0.99);
		vel.y = fabs(vel.y) * -3.0; //make the particles all be going down
	}else{
		drag  = ofRandom(0.95, 0.998);	
	}

	//m.init();

	//m.setSpacing(2);      ///Set Spacing for the mesh size

}

//------------------------------------------------------------------
void demoParticle::update(){

	//m.update();
	

	/// 1 - APPLY THE FORCES BASED ON WHICH MODE WE ARE IN 
	if( mode == PARTICLE_MODE_ATTRACT ){
		//ofVec3f screenPt = cam->worldToScreen(ofVec3f(0, 0, 0));
		ofPoint attractPt(ofGetMouseX(), ofGetMouseY());
		//ofVec3f worldPt = cam->screenToWorld(ofVec3f(attractPt.x, attractPt.y, screenPt.z));
		//attractPt.x = worldPt.x;
		//attractPt.y = worldPt.y;
		frc = attractPt-pos;                         /// we get the attraction force/vector by looking at the mouse pos relative to our pos
		frc.normalize();                            ///by normalizing we disregard how close the particle is to the attraction point 
		
		vel *= drag; //apply drag
		vel += frc * 0.6; //apply force
	
	}

	else if( mode == PARTICLE_MODE_REPEL ){
		//ofVec3f screenPt = cam->worldToScreen(ofVec3f(0, 0, 0));
		ofPoint attractPt(ofGetMouseX(), ofGetMouseY());
		//ofVec3f worldPt = cam->screenToWorld(ofVec3f(attractPt.x, attractPt.y, screenPt.z));
		//attractPt.x = worldPt.x;
		//attractPt.y = worldPt.y;
		frc = attractPt-pos;
		
		///let get the distance and only repel points close to the mouse
		float dist = frc.length();
		frc.normalize(); 
		
		vel *= drag; 
		if( dist < 150 ){
			vel += -frc * 0.6;                      ///notice the frc is negative 
		}else{
			///if the particles are not close to us, lets add a little bit of random movement using noise. this is where uniqueVal comes in handy. 			
			frc.x = ofSignedNoise(uniqueVal, pos.y * 0.01, ofGetElapsedTimef()*0.2);
			frc.y = ofSignedNoise(uniqueVal, pos.x * 0.01, ofGetElapsedTimef()*0.2);
			frc.z = 0;
			vel += frc * 0.04;
		}
	}
	else if( mode == PARTICLE_MODE_NOISE ){
		///lets simulate falling snow 
		///the fake wind is meant to add a shift to the particles based on where in x they are
		///we add pos.y as an arg so to prevent obvious vertical banding around x values - try removing the pos.y * 0.006 to see the banding
		float fakeWindX = ofSignedNoise(pos.x * 0.003, pos.y * 0.006, pos.x * 0.003, ofGetElapsedTimef() * 0.6);
		
		frc.x = fakeWindX * 0.25 + ofSignedNoise(uniqueVal, pos.y * 0.04) * 0.6;
		frc.y = ofSignedNoise(uniqueVal, pos.x * 0.006, ofGetElapsedTimef()*0.2) * 0.09 + 0.18;
		frc.z = 0;

		vel *= drag; 
		vel += frc * 0.4;
		
		/// we do this so as to skip the bounds check for the bottom and make the particles go back to the top of the screen
		if( pos.y + vel.y > ofGetHeight() ){
			pos.y -= ofGetHeight();
		}
	}
	else if( mode == PARTICLE_MODE_NEAREST_POINTS ){
		
		if( attractPoints ){

			//1 - find closest attractPoint 
			ofPoint closestPt;
			int closest = -1; 
			float closestDist = 9999999;
			
			for(unsigned int i = 0; i < attractPoints->size(); i++){
				float lenSq = ( attractPoints->at(i)-pos ).lengthSquared();
				if( lenSq < closestDist ){
					closestDist = lenSq;
					closest = i;
				}
			}
			
			/// 2 - if we have a closest point - lets calcuate the force towards it
			if( closest != -1 ){
				closestPt = attractPoints->at(closest);				
				float dist = sqrt(closestDist);
				
				//in this case we don't normalize as we want to have the force proportional to distance 
				frc = closestPt - pos;
		
				vel *= drag;
				 
				/// lets also limit our attraction to a certain distance and don't apply if 'f' key is pressed
				if( dist < 300 && dist > 40 && !ofGetKeyPressed('g') ){
					vel += frc * 0.003;
				}else{
					//if the particles are not close to us, lets add a little bit of random movement using noise. this is where uniqueVal comes in handy. 			
					frc.x = ofSignedNoise(uniqueVal, pos.y * 0.01, ofGetElapsedTimef()*0.2);
					frc.y = ofSignedNoise(uniqueVal, pos.x * 0.01, ofGetElapsedTimef()*0.2);
					frc.z = 0;
					vel += frc * 0.4;
				}
				
			}
		
		}
		
	}
	
	
	//2 - UPDATE OUR POSITION
	pos += vel; 

	
}

//------------------------------------------------------------------
void demoParticle::draw(){

	if( mode == PARTICLE_MODE_ATTRACT ){
		ofSetColor(255, ofRandom(0, 25), 55);
	}
	else if( mode == PARTICLE_MODE_REPEL ){
		ofSetColor(255, ofRandom(0, 25), 55);
	}
	else if( mode == PARTICLE_MODE_NOISE ){
		ofSetColor(255, ofRandom(0, 25), 55);
	}
	else if( mode == PARTICLE_MODE_NEAREST_POINTS ){
		ofSetColor(255, ofRandom(0,25), 55);
	}


	///the original shape
	ofDrawCircle(pos.x, pos.y, scale * 4.0);


	///// Shape and form of the particle system drawn 
	//ofPushMatrix();
	//box.set(15);
	////box.setWidth(50);
	////box.setHeight(50);
	////cout << pos.x << ", " << pos.y << ", " << pos.z << endl;
	//box.setPosition(pos.x, pos.y, pos.z);
	////mTex.bind();
	//ofSetColor(255);
	//box.draw();
	//ofPopMatrix();
	////mTex.unbind();

	//
	//ofPushMatrix();
	//ofTranslate(pos.x, pos.y, pos.z);
	//ofSetColor(ofRandom(60, 200));
	////m.drawWireframe();
	//ofPopMatrix();
	
}

