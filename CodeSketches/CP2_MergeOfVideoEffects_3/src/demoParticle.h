#pragma once
#include "ofMain.h"
//#include "Cube.h"
//#include "Mesh.h"

enum particleMode{
	PARTICLE_MODE_ATTRACT = 0,
	PARTICLE_MODE_REPEL,
	PARTICLE_MODE_NEAREST_POINTS,
	PARTICLE_MODE_NOISE
};

class demoParticle{

	public:
		//demoParticle(ofEasyCam *cam);
		demoParticle();
		
		void setMode(particleMode newMode);	
		void setAttractPoints( vector <ofPoint> * attract );

		//void setTexture(const ofTexture& tex);

		void reset();
		void update();
		void draw();		
		
		ofPoint pos;
		ofPoint vel;
		ofPoint frc;
		
		float drag; 
		float uniqueVal;
		float scale;
		
		particleMode mode;

		//ofBoxPrimitive box;
		//Cube box;
		//Mesh m;
		
		vector <ofPoint> * attractPoints; 

private:

	    //ofCamera *cam;
	    //ofEasyCam *cam;  // add mouse controls for camera movement

};