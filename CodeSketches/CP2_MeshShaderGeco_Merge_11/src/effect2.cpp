#include "effect2.h"

//--------------------------------------------------------------
void graphicEffect2::setup() {

	/// Load shader
	effect2Shader.load("effect2");          /// shader init.

	/// Movement engine by Lewis Lepton
	/// https://www.youtube.com/watch?v=XbiAGyodS3U&list=PL4neAtv21WOlqpDzGqbGM_WN2hc5ZaVv7&index=39
	speedX.resize(AMOUNT);
	speedY.resize(AMOUNT);
	speedZ.resize(AMOUNT);
	for (int i = 0; i < AMOUNT; i++) {
		speedX[i] = ofRandom(0.001, 0.9);
		speedY[i] = ofRandom(0.001, 0.9);
		speedZ[i] = ofRandom(0.001, 0.9);
	}


	/// Video Load
	cloudVid.load("FL1.mov");
	cloudVid.play();


	/// Mesh Movemet Variables
	amplitude = 2.0;
	freqTime = 10.0;
	freqSpace = 1.0;

	///CONSTELLATION
	//mesh1.setMode(OF_PRIMITIVE_LINES);
}

//--------------------------------------------------------------
void graphicEffect2::update() {

	cloudVid.update();


	/// Rotation variable update
	rot += shapeRot;
	hSize = 22;             ///hSize = 200 *ofNoise(ofGetElapsedTimef());     /// size of the side
	wSize = 22;             ///wSize = 200 *ofNoise(ofGetElapsedTimef());    
	shapeRes = 222 * ofNoise(ofGetElapsedTimef()/22);                          /// resolution of the circle in this case. the valu 4 gives square, 3 trangle, ...
	shapeRot = - 0.2;                                                         /// rotation of the shapes


	/// Mesh update 
	ellipseMesh = createEllipseWithTextureCoords(wSize, hSize, shapeRes); 



	/*/// CONSTELLATION
	/// Salvaged from Lewis Lepton's YouTube video
	///https://www.youtube.com/watch?v=wBkvusKre8Q&list=PL4neAtv21WOlqpDzGqbGM_WN2hc5ZaVv7&index=61

	uiAmount = 20;
	uiPos1 = 40.0;
	uiPos2 = 450.0;
	uiPos3 = 350.0;
	uiPos4 = 80.0;
	uiPos5 = 40.0;
	uiPos6 = 250.0;
	uiDistance = 160.0;
	uiPoints = true;
	frameSpeedMesh = 0.006;

	ofSeedRandom(30);
	mesh1.clear();
	for (int i = 0; i < uiAmount; i++) {
		
		position = ofVec3f(
			ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * frameSpeedMesh), 0, 1, uiPos1 + 2 * sin(ofGetElapsedTimef() / 4), uiPos2),
			ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * frameSpeedMesh), 0, 1, uiPos3, uiPos4),
			ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * frameSpeedMesh), 0, 1, uiPos5, uiPos6)
		);

		mesh1.addVertex(position);
		
	}

	for (int i = 0; i < mesh1.getVertices().size(); i++) {
		auto position = mesh1.getVertex(i);
		for (int j = 0; j < mesh1.getVertices().size(); j++) {
			auto jdistance = mesh1.getVertex(j);
			auto distance = glm::distance(position, jdistance);

			if (distance < uiDistance) {
				mesh1.addIndex(i);
				mesh1.addIndex(j);
			}
		}
	}*/
}




//--------------------------------------------------------------
void graphicEffect2::draw() {


	/// SHADER
	ofPushMatrix(); ///Enbraces the wole shader

	effect2Shader.begin();

		
	ofPushMatrix();
	//ofRotateZDeg(rot);
	ellipseMesh.draw();
	ofPopMatrix();

	/// Vertex shader uniforms
	effect2Shader.setUniform1f("time", ofGetElapsedTimef());
	effect2Shader.setUniform1f("amplitude", amplitude);
	effect2Shader.setUniform1f("freqTime", freqTime);
	effect2Shader.setUniform1f("freqSpace", freqSpace);

	effect2Shader.setUniform2f("u_resolution", ofGetWidth(), ofGetHeight());
	effect2Shader.setUniform2f("u_mouse", ofGetMouseX(), ofGetHeight() - ofGetMouseY());
	effect2Shader.setUniform1f("u_time", ofGetElapsedTimef());

	effect2Shader.setUniformTexture("imageTexture0", cloudVid.getTexture(), 5);            /// video texture
		
	
	effect2Shader.end();
	ofPopMatrix();  ///Enbraces the wole shader
		




	/*/// CONSTELLATION
	ofPushMatrix();
	ofTranslate(-300, -300, -100);
	if (uiPoints == 1) {
		for (int i = 0; i < mesh1.getVertices().size(); i++) {
			
			ofSetColor(255, 5, 5, ofRandom(20, 255));
			/// mwsh1 #1
			ofPushMatrix();
			ofTranslate(mesh1.getVertex(i) / 3);
			ofDrawSphere(mesh1.getVertex(i), 2 * sin(ofGetElapsedTimef() / 2));
			mesh1.draw();
			ofPopMatrix();

			/// mwsh1 #2
			ofPushMatrix();	
			ofDrawSphere(mesh1.getVertex(i), 2 * sin(ofGetElapsedTimef() / 2));
			mesh1.draw();
			ofPopMatrix();

			/// mwsh1 #3
			ofPushMatrix();
			ofTranslate(mesh1.getVertex(i) * sin(ofGetElapsedTimef())/200);
			ofDrawSphere(mesh1.getVertex(i), 2 * sin(ofGetElapsedTimef() / 2));
			mesh1.draw();
			ofPopMatrix();

		}
	}
	ofPopMatrix();*/

}




//--------------------------------------------------------------
void graphicEffect2::keyPressed(int key) {

}



ofMesh graphicEffect2::createEllipseWithTextureCoords(float xRad, float yRad, int steps) {
	ofMesh curMesh;

	curMesh.setMode(OF_PRIMITIVE_TRIANGLES);

	/// Create vertex in the middle
	curMesh.addVertex(vec3(0, 0, 0));
	curMesh.addTexCoord(vec2(0.5, 0.5));

	/// Create vertices round the circumference
	for (int i = 0; i < steps; i++) {
		float theta = ofDegToRad(360.0 * i / steps);

		/// This one gives us a circle
		curMesh.addVertex(vec3(xRad * cos(theta), yRad * sin(theta), 0));
		curMesh.addTexCoord(vec2(0.5 + 0.5 * cos(theta), 0.5 - 0.5 * sin(theta)));

	}

	/// Create triangles
	for (int i = 0; i < steps; i++) {
		if (i < steps - 1) {
			curMesh.addTriangle(0, i + 1, i + 2);
		}
		else {
			curMesh.addTriangle(0, i + 1, 1);
		}
	}


	return curMesh;
}




