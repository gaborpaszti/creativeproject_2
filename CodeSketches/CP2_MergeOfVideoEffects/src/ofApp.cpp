
#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofBackground(0);
	//transitionT;
	///CONSTELLATION
	mesh.setMode(OF_PRIMITIVE_LINES);

	/*/// GUI
	transitionT = 50 * sin(ofGetElapsedTimef() * ofNoise(ofRandom(600), ofGetElapsedTimef() * 0.006));*/

	sliderGroup.setName("sliders");
	sliderGroup.add(intSlider.set("int slider", 3, 3, 64));
	sliderGroup.add(floatSlider.set("floatSlider", 32, 32, 256));

	sliderGroup.add(uiPos1.set("uiPos1", 40 + 50 + transitionT + ofGetMouseX(), 0, 600));
	sliderGroup.add(uiPos2.set("uiPos2", 40 + 50 + transitionT + ofGetMouseY(), 0, 600));
	sliderGroup.add(uiPos3.set("uiPos3", 40 + 50 + transitionT + ofGetMouseX(), 0, 600));
	sliderGroup.add(uiPos4.set("uiPos4", 40 + 50 + transitionT + ofGetMouseY(), 0, 600));
	sliderGroup.add(uiPos5.set("uiPos5", 40 + 50 + transitionT + ofGetMouseX(), 0, 600));
	sliderGroup.add(uiPos6.set("uiPos6", 40 + 50 + transitionT + ofGetMouseY(), 0, 600));
	sliderGroup.add(uiPoints.set("uiPoints", 0, 0, 1));
	sliderGroup.add(uiDistance.set("uiDistance", 40, 0, 600));
	sliderGroup.add(uiAmount.set("uiAmount", 40 + transitionT, 0, 100));
	gui.setup(sliderGroup);
	///////

	
	
	///HAAR
	haar.setup("haarcascade_frontalface_default.xml");
	haar.setPreset(ofxCv::ObjectFinder::Fast);
	


	///WEBCAM'S
	webCam.setDeviceID(1);            /// 0 - for rear, 1 for front camera.
	webCam.setDesiredFrameRate(60);
	webCam.setup(640, 480);
	//color.allocate(webCam.getWidth(), webCam.getHeight());
	//gray.allocate(webCam.getWidth(), webCam.getHeight());
	//finder.findHaarObjects(webCam);

	////get back a list of devices.
	vector<ofVideoDevice> devices = webCam.listDevices();

	for (size_t i = 0; i < devices.size(); i++) {
		if (devices[i].bAvailable) {
			//log the device
			ofLogNotice() << devices[i].id << ": " << devices[i].deviceName;
		}
		else {
			//log the device and note it as unavailable
			ofLogNotice() << devices[i].id << ": " << devices[i].deviceName << " - unavailable ";
		}
	}


	


	imitate(pxPrevious, webCam);
	imitate(imgDifference, webCam);
}





//--------------------------------------------------------------
void ofApp::update(){

	webCam.update();


	if (webCam.isFrameNew()) {
		absdiff(webCam, pxPrevious, imgDifference);
		imgDifference.update();
		copy(webCam, pxPrevious);


	}

	


	/// CONSTELLATION
	////GUI
	ofSetCircleResolution(intSlider);
	
	/// GUI
	//transitionT;
	//transitionT = 50 * sin(ofGetElapsedTimef() * ofNoise(ofRandom(600), ofGetElapsedTimef() * 0.006));

	ofSeedRandom(10);
	mesh.clear();
	for (int i = 0; i < uiAmount; i++) {
		float frameSpeedMesh = 0.006; // *sin(ofGetElapsedTimef() / 4);
		ofVec3f position = ofVec3f(
			//ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * 0.006), 0, 1, uiPos1 , uiPos2),
			ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * frameSpeedMesh), 0, 1, uiPos1 + 2 * sin(ofGetElapsedTimef() / 4), uiPos2),
			//ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * 0.006), 0, 1, ofRandom(uiPos1 + ofGetMouseX(), uiPos1 - ofGetMouseX()), uiPos2 + ofGetMouseX()),
			ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * frameSpeedMesh), 0, 1, uiPos3, uiPos4),
			ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * frameSpeedMesh), 0, 1, uiPos5, uiPos6)
		);
		mesh.addVertex(position);
		mesh.addColor(ofColor(255));
	}

	for (int i = 0; i < mesh.getVertices().size(); i++) {
		auto position = mesh.getVertex(i);
		for (int j = 0; j < mesh.getVertices().size(); j++) {
			auto jdistance = mesh.getVertex(j);
			auto distance = glm::distance(position, jdistance);

			if (distance < uiDistance) {
				mesh.addIndex(i);
				mesh.addIndex(j);
			}
		}
	}


	///HAAR 
	if (webCam.isFrameNew()) {
		haar.update(webCam);
	}

}

//--------------------------------------------------------------
void ofApp::draw(){
	webCam.draw(0, 0);
	
	imgDifference.draw(0, 0);

	ofPushMatrix();
	ofSetColor(255, 0, 0);
	haar.draw();
	ofPopMatrix();
	ofSetColor(255);
	
	
	

	/// Video Stream Pixel Manipulation - Circle Brightness 
	for (int i = 0; i < webCam.getWidth(); i += 16) {
		//ofPolyline polyline;
		for (int j = 0; j < webCam.getHeight(); j += 16 ){    ///  j++)

			//ofColor color1 = imgDifference.getPixels().getColor(j, i);
			//int brightness = color1.getBrightness();
			
			
			ofColor color = webCam.getPixels().getColor(i, j);
			//ofSetColor(color);
			
			float brightness = color.getBrightness();
			
			float radius = ofMap(brightness, 0, 255, 0, 8);
			ofDrawCircle(i, j, radius);
			//polyline.addVertex(j, i + ofMap(brightness, 0, 255, 0, -64));

			
			
		}

		//polyline = polyline.getSmoothed(5);
		//polyline.draw();
	}


	/// Video Stream Pixel Manipulation - Polyline 
	for (int i = 0; i < webCam.getHeight(); i += 8) {
		ofPolyline polyline;
		for (int j = 0; j < webCam.getWidth(); j++) {
			ofColor color1 = imgDifference.getPixels().getColor(j, i);
			int brightness1 = color1.getBrightness();
			polyline.addVertex(j, i + ofMap(brightness1, 0, 255, 0, -254));
		}
		polyline = polyline.getSmoothed(10);
		polyline.draw();
	}

	
	///Constellation 
	///GUI
	ofPushMatrix();
	ofSetColor(255);
	ofDrawCircle(ofGetWidth() / 2, ofGetHeight() / 2, floatSlider);
	gui.draw();
	ofPopMatrix();

	


	///Constellation
	ofPushMatrix();
	ofTranslate(ofGetWidth() / 4, ofGetHeight() / 4, 200);
	if (uiPoints == 1) {
		for (int i = 0; i < mesh.getVertices().size(); i++) {
			ofSetColor(255,255,255,ofRandom(10,150));
			ofDrawSphere(mesh.getVertex(i), 2 * sin(ofGetElapsedTimef()/2));

			///Additional mesh
			/*///RED
			ofPushMatrix();
			ofTranslate(0, 0, 100);
			ofSetColor(255, 0, 0);
			ofDrawSphere(mesh.getVertex(i), 2);
			mesh.draw();
			ofPopMatrix();
			///BLACK
			ofPushMatrix();
			ofTranslate(0, 0, 150);
			ofSetColor(0, 0, 0);
			ofDrawSphere(mesh.getVertex(i), 2);
			mesh.draw();
			ofPopMatrix();*/
		}
	}
	mesh.draw();
	ofPopMatrix();

	
	

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == 'f') {
		ofToggleFullscreen();
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
