#pragma once

#include "ofMain.h"
#include "ofxCv.h"
#include "ofxGui.h"
using namespace ofxCv;
using namespace cv;

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		/// CONSTELLATION
		ofxPanel gui;
		ofParameterGroup sliderGroup;
		ofParameter<int> intSlider;
		ofParameter<float> floatSlider;

		ofParameter <float> uiAmount;
		ofParameter <float> uiPos1;
		ofParameter <float> uiPos2;
		ofParameter <float> uiPos3;
		ofParameter <float> uiPos4;
		ofParameter <float> uiPos5;
		ofParameter <float> uiPos6;
		ofParameter <float> uiDistance;
		ofParameter <bool> uiPoints;

		float transitionT;  /// the nature of movement

		///MESH
		ofMesh mesh;

		
		/// WEBCAM - HAAR
		ofVideoGrabber webCam; 
		ofxCv::ObjectFinder haar;
			
		ofImage imgDifference;
		ofPixels pxPrevious;

};
