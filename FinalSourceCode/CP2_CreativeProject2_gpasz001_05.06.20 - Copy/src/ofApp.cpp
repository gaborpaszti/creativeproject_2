#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){
	
	ofEnableDepthTest();   /// gui goes invisible dark mode when this active
	
	ofSetFrameRate(60);   /// Add this line to explicitly set the framerate to 60 frames per second
	
	
	ofDisableArbTex();
	ofEnableNormalizedTexCoords();
	ofSetVerticalSync(true);

	
	/// OSC init
	oscReceiver.setup(PORT);   /// Leap Motion (Geco Port 1234)
	
	
	/// SOUND load
	/// Loading sound with setting its properties
	soundSample0.setLoop(OF_LOOP_PALINDROME);
	soundSample0.load("MainThemeSong.mp3");
	soundSample0.setVolume(1.0);                 /// volume is active only here as this is the main track 
	//soundSample0.play();                      ///  play is active in the key pressed function
	soundSample0.setLoop(true);
	soundSample0.setSpeed(1.0f);
	soundSample0.setPan(0);
	soundSample0.setMultiPlay(false);

	/// #1
	soundSample1.load("swoosh.wav");
	//soundSample1.setVolume(0.5f);             ///  volume is active in the OSC Leap section
	//soundSample1.play();                     ///  play is active in the key pressed function
	soundSample1.setLoop(false);
	soundSample1.setSpeed(1.0f);
	soundSample1.setPan(0);
	soundSample1.setMultiPlay(false);
	/// #2
	soundSample2.load("flippingCintar.wav");
	//soundSample2.setVolume(0.5f);            ///  volume is active in the OSC Leap section
	//soundSample2.play();                    ///  play is active in the key pressed function
	soundSample2.setLoop(false);
	soundSample2.setSpeed(1.0f);
	soundSample2.setPan(0);
	soundSample1.setMultiPlay(false);
	/// #3
	soundSample3.load("eBass.wav");
	//soundSample3.setVolume(0.6f);           ///  volume is active in the OSC Leap section
	//soundSample3.play();                    ///  play is active in the key pressed function
	soundSample3.setLoop(false);
	soundSample3.setSpeed(1.0f);
	soundSample3.setPan(0);
	soundSample1.setMultiPlay(false);

	/// #4
	soundSample4.load("birdSong.mp3");
	//soundSample4.setVolume(0.8f);            ///  volume is active in the OSC Leap section
	//soundSample4.play();                    ///  play is active in the key pressed function
	soundSample4.setLoop(false);
	soundSample4.setSpeed(1.0f);
	soundSample4.setPan(0);
	soundSample1.setMultiPlay(false);


	/// IMAGE - Load
	vignetteDotsImage.load("dotCircle.jpg");
	vignetteDotsImage2.load("dotCircleB.jpg");
	starsImage.load("stars.png");


	/// SHADER - Load
	effect1Shader.load("effect1");


	
	/// WEBCAM
	webCam.setDeviceID(1);            /// 0 - for rear, 1 for front camera.
	webCam.setDesiredFrameRate(60);

	/// Get back a list of devices.
	vector<ofVideoDevice> devices = webCam.listDevices();

	for (size_t i = 0; i < devices.size(); i++) {
		if (devices[i].bAvailable) {
			///log the device
			ofLogNotice() << devices[i].id << ": " << devices[i].deviceName;
		}
		else {
			///log the device and note it as unavailable
			ofLogNotice() << devices[i].id << ": " << devices[i].deviceName << " - unavailable ";
		}
	}

	

    
	///  MESH SHADER - main big mesh on which is the shader projected
	///  https://openframeworks.cc/ofBook/chapters/generativemesh.html - this element I salvaged from this website. 
	image.load("minta2.png");           /// the base image of the mesh from which it takes the sice and saturation.
	image.resize(200, 200);     
	webCam.setup(320, 240);
	
	meshW = image.getWidth();
	meshH = image.getHeight();

	mesh.setMode(OF_PRIMITIVE_LINES);
	mesh.enableColors();

	/// We are going to be using indices this time
	mesh.enableIndices();

	float intensityThreshold = 140.0;    /// acts like an ISO - setting. mesh sensitivity 
	int w = image.getWidth();            /// here I could add fluidity to the size of the image - maybe noise
	int h = image.getHeight();       

	for (int x = 0; x < w; x++) {
		for (int y = 0; y < h; y++) {
			ofColor c = image.getColor(x, y);
			float intensity = c.getLightness();
			if (intensity >= intensityThreshold) {
				float saturation = c.getSaturation();
				float z = ofMap(saturation, 0, 255, -100, 100);
				ofVec3f shadMeshPos(x*2, y*2, z*4);   /// this line influences the size as well.
				mesh.addVertex(shadMeshPos);

				/// When addColor(...), the mesh will automatically convert
				/// the ofColor to an ofFloatColor
				mesh.addColor(c);

				/// And add this line to your existing for loop:
				/// It will create a ofVec3f with 3 random values, which
				/// will allow us to move the x, y and z coordinates of
				/// each vertex independently
				offsets.push_back(ofVec3f(ofRandom(0, 100000), ofRandom(0, 100000), ofRandom(0, 100000)));

			}
		}
	}

	/// Let's add some lines!
	float connectionDistance = 20;              /// acts like a hole maker in the net - or resolution  - dencity - bigger the number, heavier running
	int numVerts = mesh.getNumVertices();
	for (int a = 0; a < numVerts; ++a) {
		ofVec3f verta = mesh.getVertex(a);
		for (int b = a + 1; b < numVerts; ++b) {
			ofVec3f vertb = mesh.getVertex(b);
			float distance = verta.distance(vertb);
			if (distance <= connectionDistance) {
				/// In OF_PRIMITIVE_LINES, every pair of vertices or indices will be
				/// connected to form a line
				mesh.addIndex(a);
				mesh.addIndex(b);
			}
		}
	}


	

	/// CONSTELLATION
	mesh1.setMode(OF_PRIMITIVE_LINES);


	/// GUI
	ofPushMatrix();

	sliderGroup.setName("CONTROLLS");

	/// 2D Primitive circle resolution) Gui
	sliderGroup.add(intSlider.set("int slider", 3, 3, 64));
	sliderGroup.add(floatSlider.set("floatSlider", 1.0, 1.0, 256.0));

	/// Constellation Gui
	sliderGroup.add(uiPos1.set("uiPos1", 40.0, 0.0, 600.0));
	sliderGroup.add(uiPos2.set("uiPos2", 450.0, 0, 600));
	sliderGroup.add(uiPos3.set("uiPos3", 350.0, 0.0, 600.0));
	sliderGroup.add(uiPos4.set("uiPos4", 80.0, 0.0, 600.0));
	sliderGroup.add(uiPos5.set("uiPos5", 40.0, 0.0, 600.0));
	sliderGroup.add(uiPos6.set("uiPos6", 250.0, 0.0, 600.0));
	sliderGroup.add(uiPoints.set("uiPoints", 1.0, 0.0, 1.0));
	sliderGroup.add(uiDistance.set("uiDistance", 70.0, 0.0, 500.0));
	sliderGroup.add(uiAmount.set("uiAmount", 20.0, 0.0, 100.0));
	sliderGroup.add(frameSpeedMesh.set("frameSpeedMesh", 0.006, 0.0, 10.0));

	/// Shader Mesh Gui
	sliderGroup.add(timeScale.set("timeScale", 9.0, 0.0, 20.0));
	sliderGroup.add(displacementScale.set("displacementScale", 0.75, 0.0, 10.0));

	/// Sound Gui
	sliderGroup.add(volume.set("volume", 0.9, 0.0, 1.0));
	sliderGroup.add(decay.set("decay", 0.5, 0.0, 1.0));

	/// Light - Gui
	sliderGroup.add(uiPosition.set("position",ofVec3f(0.0, 0.0, 500.0),ofVec3f(-10000.0, -10000.0, -10000.0),ofVec3f(10000.0, 10000.0, 10000.0)));

	/// Rotation - Gui
	sliderGroup.add(uiRotationX.set("rotationX", 0.0, 0.0, 5.0));
	sliderGroup.add(uiRotationY.set("rotationY", 0.15, 0.0, 5.0));
	sliderGroup.add(uiRotationZ.set("rotationZ", 0.01, 0.0, 5.0));

	/// Width scale - used as a unit
	sliderGroup.add(unitWidth.set("Width", 4000.0, 0.0, 4000.0));
	/// Height scale - used as a unit
	sliderGroup.add(unitHeight.set("Height", 255.0, 0.0, 2500.0));
	/// Height scale - used as a unit
	sliderGroup.add(unitDepth.set("Depth", 540.0, -10000.0, 10000.0));

	gui.setup(sliderGroup);

	drawCommands = false;  /// Bool for drawing the gui. By default is off.
	ofPopMatrix();



	/// FFT
	fft = new float[128];
	for (int i = 0; i < 128; i++) {
		fft[i] = 0;
	}
	bands = 64;



	/// CAMERA Observer
	camera.setPosition(0.0, 0.0, 540.0);
	camera.setTarget(vec3(0.0, 4.0, 0.0));
	camera.setFov(32.0);
	camera.setAutoDistance(false);



	///MATERIAL - Initialize materials
	whiteMaterial.setDiffuseColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	whiteMaterial.setSpecularColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	whiteMaterial.setAmbientColor(ofFloatColor(1.0f, 0.2f, 0.1f));
	whiteMaterial.setShininess(100.0);

	redMaterial.setDiffuseColor(ofFloatColor(1.0f, 0.2f, 0.1f));
	redMaterial.setSpecularColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	redMaterial.setAmbientColor(ofFloatColor(1.0f, 0.2f, 0.1f));
	redMaterial.setShininess(100.0);

	greenMaterial.setDiffuseColor(ofFloatColor(0.2f, 1.0f, 0.2f));
	greenMaterial.setSpecularColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	greenMaterial.setAmbientColor(ofFloatColor(0.2f, 1.0f, 0.2f));
	greenMaterial.setShininess(50.0);

	blueMaterial.setDiffuseColor(ofFloatColor(0.1f, 0.2f, 1.0f));
	blueMaterial.setSpecularColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	blueMaterial.setAmbientColor(ofFloatColor(0.1f, 0.2f, 1.0f));
	blueMaterial.setShininess(10.0);

	///LIGHT - Initialize lights
	pointLight.setDiffuseColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	pointLight.setSpecularColor(ofFloatColor(1.0f, 1.0f, 1.0f));
	pointLight.setPosition(vec3(410.0f, 410.0f, 600.0f));

	ofSetGlobalAmbientColor(ofFloatColor(0.1f, 0.1f, 0.1f));

	pointLight.enable();

	
	/// Initialize primitives
	myBox.setWidth(9.0f);
	myBox.setHeight(9.0f);
	myBox.setDepth(9.0f);

	
	/// EFFECT 2
	myGraphicEffect2.setup();

	/// Effect 2 Switch
	effectSwitch2 = false;



	ofEnableNormalizedTexCoords();
}





/// UPDATE   --------------------------------------------------------------
void ofApp::update(){

	plusPlus += 1;    /// this global variable is acting as a ++ 
	minusMinus -= 1; /// this global variable is acting as a --


	/// EFFECT 2
	myGraphicEffect2.update();

	/// LIGHT
	pointLight.setPosition(uiPosition->x, uiPosition->y, uiPosition->z);   /// position of the light source - adjustable on the XYZ axis via slider.
	
	/// WEBCAM
	webCam.update();

	///SOUND
	ofSoundUpdate();
	soundSample0.setVolume(volume);
	soundSpectrum = ofSoundGetSpectrum(bands);
	for (int i = 0; i < bands; i++) {
		fft[i] *= decay;
		if (fft[i] < soundSpectrum[i]) {
			fft[i] = soundSpectrum[i];
		}
	}


	/// SHADER MESH
	int numVerts = mesh.getNumVertices();
	for (int i = 0; i < numVerts; ++i) {
		ofVec3f vert = mesh.getVertex(i);

		float time = ofGetElapsedTimef();
		//float timeScale = 9.0;              /// increases or decreases the movement in time of the mesh - frequency -  added a slider for it
		//float displacementScale = 0.75;    /// increases or decreases the movement in space of the mesh - amplitude -  added a slider for it
		ofVec3f timeOffsets = offsets[i];

		/// A typical design pattern for using Perlin noise uses a couple parameters:
		/// ofSignedNoise(time*timeScale+timeOffset)*displacementScale
		///     ofSignedNoise(time) gives us noise values that change smoothly over
		///         time
		///     ofSignedNoise(time*timeScale) allows us to control the smoothness of
		///         our noise (smaller timeScale, smoother values)
		///     ofSignedNoise(time+timeOffset) allows us to use the same Perlin noise
		///         function to control multiple things and have them look as if they
		///         are moving independently
		///     ofSignedNoise(time)*displacementScale allows us to change the bounds
		///         of the noise from [-1, 1] to whatever we want
		/// Combine all of those parameters together, and you've got some nice
		/// control over your noise

		/// adding noise to the mesh movement
		vert.x += (ofSignedNoise(time*timeScale + timeOffsets.x)) * displacementScale; 
		vert.y += (ofSignedNoise(time*timeScale + timeOffsets.y)) * displacementScale;
		vert.z += (ofSignedNoise(time*timeScale + timeOffsets.z)) * displacementScale;

		mesh.setVertex(i, vert);
	}


	/// OSC Leap Motion
	/// This OSC is connected to Leap Motion through Geco app
	while (oscReceiver.hasWaitingMessages()) {
		ofxOscMessage msg;
		oscReceiver.getNextMessage(msg);



		/// OSC LEAP Up-Down reading with Closed/Open LEFT HAND
		if (msg.getAddress() == "/left/open/movy" || msg.getAddress() == "/left/closed/movy") {

			/// Constellation
			uiDistance = msg.getArgAsFloat(0);
			uiDistance = ofMap(uiDistance, 0, 1, 0, 600);

			/// Volume
			volume = msg.getArgAsFloat(0);
			volume = ofMap(volume, 0, 1, 0, 1);     

			soundSample1.setVolume(volume);
			soundSample2.setVolume(volume);
			soundSample3.setVolume(volume);
			soundSample4.setVolume(volume);
		}


		/// OSC LEAP Left - Right reading with Closed/Open LEFT HAND
		if (msg.getAddress() == "/left/open/movx" || msg.getAddress() == "/left/closed/movx") {

			/// unitHeight - slider
			unitHeight = msg.getArgAsFloat(0);
			unitHeight = ofMap(unitHeight, 0, 1, 250, 2500);
		}

		
		/// OSC LEAP Backward - Forward reading with Closed/Open LEFT HAND
		if (msg.getAddress() == "/left/open/movz" || msg.getAddress() == "/left/closed/movz") {

			unitDepth = msg.getArgAsFloat(0);
			unitDepth = ofMap(unitDepth, 0, 1, -10000, 10000);
			camera.setDistance(unitDepth);
		}

		/// ROTATION COMMANDS
		/// OSC LEAP Pitch Inclination reading with Closed/Open LEFT HAND
		if (msg.getAddress() == "/left/closed/pitch" || msg.getAddress() == "/left/open/pitch") {

			/// uiRotationZ - slider
			uiRotationX = msg.getArgAsFloat(0);
			uiRotationX = ofMap(uiRotationX, 0, 1, 0, 0.1);
		}

		/// OSC LEAP Yaw Inclination reading with Closed/Open LEFT HAND
		if (msg.getAddress() == "/left/open/yaw" || msg.getAddress() == "/left/closed/yaw") {

			/// uiRotationY - slider
			uiRotationY = msg.getArgAsFloat(0);
			uiRotationY = ofMap(uiRotationY, 0, 0.3, 0, 0.1);
		}

		/// OSC LEAP Roll Inclination reading with Closed/Open LEFT HAND
		if (msg.getAddress() == "/left / closed / roll" || msg.getAddress() == "/left/open/roll") {

			/// uiRotationZ - slider
			uiRotationZ = msg.getArgAsFloat(0);
			uiRotationZ = ofMap(uiRotationZ, 0, 0.3, 0, 0.1);
		}


	}/// END OF OSC
	


	/// CONSTELLATION - Mesh
	/// Salvaged from Lewis Lepton's YouTube video
	/// https://www.youtube.com/watch?v=wBkvusKre8Q&list=PL4neAtv21WOlqpDzGqbGM_WN2hc5ZaVv7&index=61 by Lewis Lepton
	ofSeedRandom(30);
	mesh1.clear();
	for (int i = 0; i < uiAmount; i++) {
		///float frameSpeedMesh = 0.006;    /// this value is implemented in slider 
		position = ofVec3f(
			ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * frameSpeedMesh), 0, 1, uiPos1 + 2 * sin(ofGetElapsedTimef() / 4), uiPos2),
			ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * frameSpeedMesh), 0, 1, uiPos3, uiPos4),
			ofMap(ofNoise(ofRandom(600), ofGetElapsedTimef() * frameSpeedMesh), 0, 1, uiPos5, uiPos6)
		);
		mesh1.addVertex(position);
	}

	for (int i = 0; i < mesh1.getVertices().size(); i++) {
		auto position = mesh1.getVertex(i);
		for (int j = 0; j < mesh1.getVertices().size(); j++) {
			auto jdistance = mesh1.getVertex(j);
			auto distance = glm::distance(position, jdistance);

			if (distance < uiDistance) {
				mesh1.addIndex(i);
				mesh1.addIndex(j);
			}
		}
	}


	/// GUI
	ofSetCircleResolution(intSlider);
	
}

//--------------------------------------------------------------
void ofApp::draw(){
	

	/// BACKGROUND
	ofColor centerColor = ofColor(5, 0, 11);    
    ofColor edgeColor(0, 0, 0, ofRandom(255, 200));
	ofBackgroundGradient(centerColor, edgeColor, OF_GRADIENT_CIRCULAR);     /// this background gradient mode gives a certain mood to the work
	ofClear(0, 0, 0, 1);
	


	///GUI - needs to be here at the top wrapped in ofDisableDepthTest()
	ofPushMatrix();
	ofDisableDepthTest();
	if (drawCommands == true) {
		gui.draw();
	}
	ofPopMatrix();

	


	ofEnableDepthTest();

	/// LIGHT - START
	ofEnableLighting();

	/// CAMERA - START
	camera.begin();


	/// ROTATION - of the whole draw
	ofRotateXDeg(uiRotationX * plusPlus);
	ofRotateYDeg(uiRotationY * plusPlus);
	ofRotateZDeg(uiRotationZ * plusPlus);


	/// CUBE 3D primitive at the center  - on the grid
	whiteMaterial.begin();
	starsImage.bind();
	ofPushMatrix();
	myBox.draw();
	ofPopMatrix();
	starsImage.unbind();
	whiteMaterial.end();


	/// GRID for orientation and aesthetic
	ofDrawGrid(1, 5, false, true, true, true);


	/// EFFECT 2 
	if (effectSwitch2 == true) {
		ofPushMatrix();
		ofTranslate(-0, -0, -150);
		myGraphicEffect2.draw();
		ofPopMatrix();
	}



	/// CONSTELLATION
	ofPushMatrix();
	ofTranslate(-300, -300, -400);  ///100*fft[i]
	whiteMaterial.begin();
	if (uiPoints == 1) {
		for (int i = 0; i < mesh1.getVertices().size(); i++) {

			/// mesh1 #1     
			ofPushMatrix();  /// all mesh1 #1
			ofTranslate(mesh1.getVertex(i) / 3);

			ofPushMatrix();  /// myBox
			starsImage.bind();
			ofTranslate(mesh1.getVertex(i));
			ofScale(3* fft[i], 3 * fft[i], 3 * fft[i]);
			myBox.draw();
			starsImage.unbind();
			ofPopMatrix();      /// myBox

			mesh1.addColor(ofFloatColor(255.0, 0.0, 0.0, ofRandom(10, 200)));  // add a color at that vertex
			mesh1.draw();
			ofPopMatrix();   /// all mesh1 #1



			/// mesh1 #2     
			ofPushMatrix();        /// all mesh1 #2
			ofTranslate(mesh1.getVertex(i) / 4);

			ofPushMatrix();       /// myBox
			starsImage.bind();
			ofTranslate(mesh1.getVertex(i));
			ofScale(3 * fft[i], 3 * fft[i], 3 * fft[i]);
			myBox.draw();
			starsImage.unbind();
			ofPopMatrix();      /// myBox

			ofTranslate(30 * fft[i], 30 * fft[i], 30 * fft[i]);
			mesh1.addColor(ofFloatColor(255.0, 0.0, 0.0, ofRandom(10, 200)));  // add a color at that vertex
			mesh1.draw();
			ofPopMatrix();  /// all mesh1 #2



			/// mesh1 #3  
			ofPushMatrix();        /// all mesh1 #3

			ofPushMatrix();        /// myBox
			starsImage.bind();
			ofTranslate(mesh1.getVertex(i));
			ofScale(3 * fft[i], 3 * fft[i], 3 * fft[i]);
			myBox.draw();
			starsImage.unbind();
			ofPopMatrix();        /// myBox

			ofTranslate(30 * fft[i], 30 * fft[i], 30 * fft[i]);
			mesh1.addColor(ofFloatColor(255.0, 0.0, 0.0, ofRandom(10, 200)));  // add a color at that vertex
			mesh1.draw();
			ofPopMatrix();   /// all mesh1 #3

		}
	}
	whiteMaterial.end();
	ofPopMatrix();



	/// SHADER START
	ofPushMatrix();
	ofTranslate(0 - meshW, 0 - meshH, 0);   /// this is necessary because the shader's mesh 000 is from bottom left
	effect1Shader.begin();

	///UNIFORM TEXTURES
	effect1Shader.setUniformTexture("imageTexture", webCam.getTexture(), 1);               /// webCam
	effect1Shader.setUniformTexture("imageTexture1", vignetteDotsImage.getTexture(), 2);   /// vignette dots
	effect1Shader.setUniformTexture("imageTexture2", starsImage.getTexture(), 3);          /// stars 
	
	/// UNIFORM VARIABLES
	effect1Shader.setUniform1f("u_time", ofGetElapsedTimef());
	effect1Shader.setUniform2f("mousePos", vec2(ofGetMouseX(), ofGetHeight() - ofGetMouseY()));
	effect1Shader.setUniform2f("rectangleSize", vec2(unitWidth, unitHeight));                       /// Shader window size
	effect1Shader.setUniform2f("u_resolution", webCam.getWidth(), webCam.getHeight());

	ofPushMatrix();
	mesh.draw();
	ofPopMatrix();
	
	ofDrawGrid(2.0, 10, false, false, true, false);  /// This grid is the bottom let grid for orientation and aesthetic
	
	/// SHADER END
	effect1Shader.end();
	ofPopMatrix();



	
	/// CAMERA - END
	camera.end();

	///LIGHT - END
    ofDisableLighting();


	ofDisableDepthTest();

}



//--------------------------------------------------------------
void ofApp::keyPressed(int key){

	/// FULL SCREEN
	if (key == 'f') {
		ofToggleFullscreen();
	}

	/// GUI - draw commands
	if (key == 'h') {
		drawCommands = true;
		ofShowCursor();
		effectSwitch2 = true;
	}
	
	
	/// SOUND
	if (key == '0') {
		soundSample0.play();
	}
	if (key == '1') {
		soundSample1.play();
	}

	if (key == '2') {
		soundSample2.play();
	}

	if (key == '3') {
		soundSample3.play();
	}

	if (key == '4') {
		soundSample4.play();
	}

}
                           

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

	///GUI - Hide commands
	if (key == 'h') {
	drawCommands = false;
	ofHideCursor();
	effectSwitch2 = false;
	}

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}

