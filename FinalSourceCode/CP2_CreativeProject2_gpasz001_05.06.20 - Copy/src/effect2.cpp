#include "effect2.h"

//--------------------------------------------------------------
void graphicEffect2::setup() {

	/// SHADER - load
	effect2Shader.load("effect2");          /// shader init.

	/// Movement engine by Lewis Lepton
	/// https://www.youtube.com/watch?v=XbiAGyodS3U&list=PL4neAtv21WOlqpDzGqbGM_WN2hc5ZaVv7&index=39
	speedX.resize(AMOUNT);
	speedY.resize(AMOUNT);
	speedZ.resize(AMOUNT);
	for (int i = 0; i < AMOUNT; i++) {
		speedX[i] = ofRandom(0.001, 0.9);
		speedY[i] = ofRandom(0.001, 0.9);
		speedZ[i] = ofRandom(0.001, 0.9);
	}


	/// Video - load
	cloudVid.load("FL1.mov");
	cloudVid.play();


	/// Mesh Movemet Variables
	amplitude = 2.0;
	freqTime = 10.0;
	freqSpace = 1.0;

}

//--------------------------------------------------------------
void graphicEffect2::update() {

	cloudVid.update();


	/// Rotation variable update
	rot += shapeRot;
	hSize = 22;             ///hSize = 200 *ofNoise(ofGetElapsedTimef());     /// size of the side
	wSize = 22;             ///wSize = 200 *ofNoise(ofGetElapsedTimef());    
	shapeRes = 222 * ofNoise(ofGetElapsedTimef()/22);                          /// resolution of the circle in this case. the valu 4 gives square, 3 triangle, ...
	shapeRot = - 0.2;                                                         /// rotation of the shapes


	/// Mesh update 
	ellipseMesh = createEllipseWithTextureCoords(wSize, hSize, shapeRes); 

}




//--------------------------------------------------------------
void graphicEffect2::draw() {


	/// SHADER
	ofPushMatrix(); ///Enbraces the wole shader

	effect2Shader.begin();

		
	ofPushMatrix();
	ellipseMesh.draw();
	ofPopMatrix();

	/// Vertex shader uniforms
	effect2Shader.setUniform1f("time", ofGetElapsedTimef());
	effect2Shader.setUniform1f("amplitude", amplitude);
	effect2Shader.setUniform1f("freqTime", freqTime);
	effect2Shader.setUniform1f("freqSpace", freqSpace);

	effect2Shader.setUniform2f("u_resolution", ofGetWidth(), ofGetHeight());
	effect2Shader.setUniform2f("u_mouse", ofGetMouseX(), ofGetHeight() - ofGetMouseY());
	effect2Shader.setUniform1f("u_time", ofGetElapsedTimef());

	effect2Shader.setUniformTexture("imageTexture0", cloudVid.getTexture(), 5);            /// video texture
		
	
	effect2Shader.end();
	ofPopMatrix();         ///Enbraces the wole shader

}


//--------------------------------------------------------------
void graphicEffect2::keyPressed(int key) {

}



ofMesh graphicEffect2::createEllipseWithTextureCoords(float xRad, float yRad, int steps) {
	ofMesh curMesh;

	curMesh.setMode(OF_PRIMITIVE_TRIANGLES);

	/// Create vertex in the middle
	curMesh.addVertex(vec3(0, 0, 0));
	curMesh.addTexCoord(vec2(0.5, 0.5));

	/// Create vertices round the circumference
	for (int i = 0; i < steps; i++) {
		float theta = ofDegToRad(360.0 * i / steps);

		/// This one gives us a circle
		curMesh.addVertex(vec3(xRad * cos(theta), yRad * sin(theta), 0));
		curMesh.addTexCoord(vec2(0.5 + 0.5 * cos(theta), 0.5 - 0.5 * sin(theta)));

	}

	/// Create triangles
	for (int i = 0; i < steps; i++) {
		if (i < steps - 1) {
			curMesh.addTriangle(0, i + 1, i + 2);
		}
		else {
			curMesh.addTriangle(0, i + 1, 1);
		}
	}


	return curMesh;
}




